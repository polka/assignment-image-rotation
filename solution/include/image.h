#ifndef IMAGE
#define IMAGE

#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>

struct image{
	struct pixel* data;
	uint64_t width, heigth;
};

struct __attribute__((packed)) pixel{
	uint8_t r, g, b;
};

struct pixel* get_pixel(struct image *image, uint64_t a, uint64_t b);

struct image create_image(uint64_t width, uint64_t heigth);
void free_image(struct image *img);

#endif
