#ifndef STATUSES
#define STATUSES


enum read_status {
	READ_OK = 0,
	READ_INVALID_HEADER,
	READ_INVALID_PIXELS,
	READ_ERROR
};

enum write_status {
	WRITE_OK = 0,
	WRITE_HEADER_ERROR,
	WRITE_PIXELS_ERROR,
	WRITE_ERROR
};

enum open_status {
	OPEN_OK = 0, 
	OPEN_ERROR
};

#endif
