#ifndef ROTATE_H
#define ROTATE_H

#include "image.h"

extern int rotate_bmp_image(char* path_to_file, char* path_to_result);

#endif
