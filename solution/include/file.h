#ifndef UTIL_H
#define UTIL_H

#include "status.h"
#include <stdio.h>

extern enum open_status open_file(FILE** file, const char* path, const char* mode);
extern int close_file(FILE* file);

#endif
