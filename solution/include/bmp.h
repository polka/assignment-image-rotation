#ifndef BMP_H
#define BMP_H

#include "image.h"
#include "status.h"
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>

#define TYPE 0x4D42
#define SIZE 40
#define PLANES 1
#define OFFSET 54
#define COUNT 24

extern struct bmp_header default_bmp_header(size_t width, size_t heigth);

extern enum read_status from_bmp(FILE* in, struct image *img);
extern enum write_status to_bmp(FILE* out, struct image const *img);

extern enum read_status read_bmp_header(FILE* in, struct bmp_header* header);
extern enum read_status read_pixels(struct image* img, FILE* in);

extern enum write_status write_bmp_header(FILE* out, struct bmp_header* header);
extern enum write_status write_pixels(struct image const* img, FILE* out);

#endif

