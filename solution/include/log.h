#ifndef LOG
#define LOG

#include "status.h"

extern void log_read_msg(enum read_status s);
extern void log_write_msg(enum write_status s);
extern void log_open_msg(enum open_status s);

extern void err();
extern void out();

#endif
