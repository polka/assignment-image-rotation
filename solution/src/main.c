#include "../include/rotate.h"


int main( int argc, char** argv ) {
	if(argc != 3){
		printf("Wrong num of arguments, you only have to pass two files (bmp) as input to the program");
    		return 1;
	}
	

	char* path_to_file = argv[1];
	char* path_to_result = argv[2];
	
	return rotate_bmp_image(path_to_file, path_to_result);

}
