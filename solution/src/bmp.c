#include "../include/bmp.h"
#include <malloc.h>

struct __attribute__((packed)) bmp_header{
	uint16_t bfType;
	uint32_t bfileSize;
	uint32_t bfReserved;
	uint32_t bOffBits;
	uint32_t biSize;
	uint32_t biWidth;
	uint32_t biHeight;
	uint16_t biPlanes;
	uint16_t biBitCount;
	uint32_t biCompression;
	uint32_t biSizeImage;
	uint32_t biXPelsPerMeter;
	uint32_t biYPelsPerMeter;
	uint32_t biClrUsed;
	uint32_t biClrImportant;
};

struct bmp_header default_bmp_header(size_t width, size_t heigth){
	
	struct bmp_header bh;
	
	bh.bfType = TYPE;
	bh.bfileSize = width*heigth*COUNT;
	bh.bfReserved = 0;
	bh.bOffBits = OFFSET;
	bh.biSize = SIZE;
	bh.biWidth = width;
	bh.biHeight = heigth;
	bh.biPlanes = PLANES;
	bh.biBitCount = COUNT;
	bh.biCompression = 0;
	bh.biSizeImage = sizeof(struct bmp_header) + width*heigth*COUNT;
	bh.biXPelsPerMeter = 0;
	bh.biYPelsPerMeter = 0;
	bh.biClrUsed = 0;
	bh.biClrImportant = 0;
	
	return bh;
}

enum read_status read_bmp_header(FILE* in, struct bmp_header* header){
	size_t status = fread(header, sizeof(struct bmp_header), 1, in);
	if(status!=1) return READ_INVALID_HEADER;
	if(header->bfType != TYPE) return READ_INVALID_HEADER;
	return READ_OK;
}

enum read_status read_pixels(struct image* img, FILE* in){
	size_t h = img -> heigth;
	size_t w = img -> width;
	uint32_t padding = (4 - ((w*3)%4))%4;
	
	for(size_t i = 0; i < h; i++){
		size_t status = fread(&img -> data[i*w], sizeof(struct pixel), w, in);
		if((status!=w) || (fseek(in, padding, SEEK_CUR)) ){
			free_image(img);
			return READ_INVALID_PIXELS;
		}
	}
	return READ_OK;
}


enum read_status from_bmp(FILE* in, struct image *img){
	struct bmp_header bh;
	
	enum read_status status = read_bmp_header(in, &bh);
	if (status != 0) return status;
	if (fseek(in, bh.bOffBits, SEEK_SET)) return READ_ERROR;
	
	*img = create_image(bh.biWidth, bh.biHeight);
	
	return read_pixels(img, in);
}

enum write_status write_header(FILE *out, struct bmp_header* bh){
	if(!fwrite(bh, sizeof(struct bmp_header), 1, out)) return WRITE_HEADER_ERROR;
	return WRITE_OK;
}

enum write_status write_pixels(struct image const* img, FILE* out){
	size_t h = img -> heigth;
	size_t w = img -> width;
	uint32_t padding = (4 - ((w*3)%4))%4;
	
	for (size_t i = 0; i < h; i++){
		if((fwrite(&img -> data[i*w], sizeof(struct pixel), w, out))!=w){
			return WRITE_PIXELS_ERROR;
		}
		if (fseek(out, padding, SEEK_CUR)) return WRITE_PIXELS_ERROR;
	}
	return WRITE_OK;	
}

enum write_status to_bmp(FILE* out, struct image const* img){
	struct bmp_header bh = default_bmp_header(img -> width, img -> heigth);
	if (write_header(out, &bh) != 0) {
		return WRITE_ERROR;
	}
	if(fseek(out, bh.bOffBits, SEEK_SET)) return WRITE_ERROR;
	
	return write_pixels(img, out);
}








