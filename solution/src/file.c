#include "../include/file.h"

enum open_status open_file(FILE** file, const char *path, const char *mode){
	*file = fopen(path, mode);
	if (*file != NULL) return OPEN_OK;
	return OPEN_ERROR;
}
	
int close_file(FILE* f){
	return fclose(f);
}
