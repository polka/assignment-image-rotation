#include "../include/bmp.h"
#include "../include/file.h"
#include "../include/image.h"
#include "../include/log.h"
#include "rotate.h"


void rotate_struct_image_90(struct image *source, struct image *rotated){

	for (size_t i = 0; i < source -> heigth; i++){
		for (size_t j = 0; j < source -> width; j++){
			*get_pixel(rotated, source -> heigth - 1 - i, j) = *get_pixel(source, j, i); 
		}
	}
}

int rotate_bmp_image(char* path_to_file, char* path_to_result){
	FILE *in = NULL, *out = NULL;
	struct image in_image;
	
	enum open_status open_status = open_file(&in, path_to_file, "rb");
	
	log_open_msg(open_status);
	if (open_status != OPEN_OK) return 1;

	enum read_status read_status = from_bmp(in, &in_image);
	log_read_msg(read_status);
	
	if(close_file(in) != 0){
		printf("Error while closing file");
    		return 1;	
	}
	
	if (read_status != READ_OK) return 1;
	
	struct image out_image = create_image(in_image.heigth, in_image.width);
	rotate_struct_image_90(&in_image, &out_image);
	
	open_status = open_file(&out, path_to_result, "wb");
	
	log_open_msg(open_status);
	free_image(&in_image);
	
	if (open_status != OPEN_OK) return 1;
	
	enum write_status write_status = to_bmp(out, &out_image);
	
	free_image(&out_image);
	log_write_msg(write_status);
	
	if(close_file(out) != 0){
		printf("Error while closing file");
    		return 1;	
	}
	
	if (write_status != WRITE_OK) return 1;
	
	return 0;
}

