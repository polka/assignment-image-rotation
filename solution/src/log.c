#include "log.h"
#include <stdio.h>

void out(char* info){
	printf("INFO: %s", info);
}
void err(char* error){
	printf("ERROR: %s", error);
}

void log_read_msg(enum read_status s) {
	switch(s) {
		case READ_OK:
			out("file was successfully read\n");
			break;
		case READ_INVALID_HEADER:
			err("reading invalid file header\n");
			break;
		case READ_INVALID_PIXELS:
			err("reading invalid pixels\n");
			break;
		case READ_ERROR:
		default:
			err("some error occured while reading\n");
			break;
	}
}

void log_write_msg(enum write_status s) {
	switch(s) {
		case WRITE_OK:
			out("file was successfully written\n");
			break;
		case WRITE_HEADER_ERROR:
			err("problems with writing file header\n");
			break;
		case WRITE_PIXELS_ERROR:
			err("problems with writing pixels\n");
			break;
		case WRITE_ERROR:
		default:
			err("some error occured while writing\n");
			break;
	}
}

void log_open_msg(enum open_status s) {
	switch(s) {
		case OPEN_OK:
			out("file was successfully opened\n");
			break;
		case OPEN_ERROR:
			err("some error occured while opening file\n");
			break;
	}
}

