#include "../include/image.h"
#include <malloc.h>

struct pixel* get_pixel(struct image *image, uint64_t a, uint64_t b){
	return &image -> data[b*image -> width + a];
}

struct image create_image(uint64_t width, uint64_t heigth){
	return (struct image)
	{
	.data = malloc(sizeof(struct pixel) * width * heigth), 
	.width = width, 
	.heigth = heigth
	};
}
void free_image(struct image *img){
	free(img->data);
}
